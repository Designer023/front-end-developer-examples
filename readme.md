# Overview


## Understandable websites

Making a website understandable is pretty easy, we just need to write in a way that humans can understand. Unfortunately most of your users come via search, and as much as search engines are good at getting information, they don’t really understand it that well at all.

Search engines guess and assume by picking out data that they think you’re after. For example if you have text that says ‘Grand Opening on 23rd May’ a person will know it’s referencing an event. A search engines can only assume. We need to give them machines a helping hand, so they can help us further down the line.

[Let's make webpages more readable for search engines](seo.md)


## Accessible websites

So now that machines can read understand our sites lets think about the human visitors again. Machines have no issue with taking in information, other than being able to understanding it (covered above). Humans on the other hand come in all shapes and sizes and have all number of problems to go along with. There is a massive list of disabilities that can affect web users:

- Blindness, color blindness and partial sightedness causing readability issues.
- Deafness meaning problems with audio multimedia.
- Cognitive disabilities causing understanding of information.
- Physical disabilities preventing or slowing interaction.

[Let's make webpages more readable for humans of all shapes and sizes](accessibility.md)

## High performance websites

Great! We now have sites that are understandable by both human and machine visitor. Anyone looking for content can get exactly what they are looking for, but sadly there is another problem we need to fix.

These days there’s a trend to create websites with massive images or video backgrounds, loads of awesome plugins and HD multimedia. That’s cool, and it’s what we want, but when it’s done badly there can be tens of Megabytes of data and hundreds of requests back and to from the server just to show the basic page. That means slow ass websites, especially on slow or flaky internet connections.

[Let's make webpages that are fast to load](speed-optimisation.md)

## Social websites

People like to tell stories and share things that that like. Giving people the ability to do that is a big step in enabling sharing. The ability to share is not enough. People like to share things that look nice in their social feeds.

[Let's make webpages that are nice to share](social-sharing.md)
