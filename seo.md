# SEO

Being able to find a website is a great feature. By helping Google search engine (other brands available) to be able to navigate and index a website with context of what that page contains is vital.

- The page should have a hierarchy
- There should be only one h1.
- All page elements and modules should use h2 and below.
- Pagination should rel and next and canonical tags in the header of a page, along side rel and next in the actual pagination code
- Pages should have a h1 tag on the page that is descriptive of the page e.g. Carl's Profile  not just Profile
- Page titles should be between 50-60 characters in length
- Schema should be used on all relevant content - [Blogs](https://schema.org/BlogPosting), [Articles](https://schema.org/Article), [listings](https://schema.org/docs/search_results.html#q=listing), [recipies](https://schema.org/Recipe), etc

**Extra reading:**

- https://schema.org/
- https://googlewebmastercentral.blogspot.co.uk/2011/09/pagination-with-relnext-and-relprev.html
