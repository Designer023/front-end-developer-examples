# Speed and optimisation

- Critical CSS should be used to load styles above 'the fold' where possible
- CSS and Javascript should be minified
- CSS should be loaded in the head
- Javascript in the footer!!! Including Ad inits for DFP and Analytics, Facebook, Twitter. These must render after the page content has loaded. If it's not essential for the page and it's presentation then it's in the footer.
- Javascript exceptions for the header can be font services and  Modernizr
- Images should be optimized and responsive - Use srcset where appropriate
- Where external resources are used a DNS prefetch should be applied to reduce lookup times
- Check the page load waterfall to spot any issues with resources (web page speed test)

**Tools**

- https://www.smashingmagazine.com/2015/08/understanding-critical-css/
- http://www.webpagetest.org/
