#Social and sharing

Since most modern operating systems have social sharing built in a root level, it's not always necessary to add sharing buttons. What is more important is add meta data to pages so that sharing services (Facebook, Twitter...) can pull out desired information enabling them to show content in a rich format with nice big preview images or fancy twitter cards.

- Facebook OpenGraph and Twitter cards should be on all relevant pages that have a matching OG type (articles, blogs, videos)
- There should be meta descriptions on pages as a fallback for social services to pull out data from
- favicons and icons should be made available so that brand recognition can be achieved


**Extra reading**

- https://dev.twitter.com/cards/overview
- https://developers.facebook.com/docs/sharing/best-practices#tags
- https://realfavicongenerator.net/
