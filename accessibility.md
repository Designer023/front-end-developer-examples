# Accessibility and useability

Not everyone can see or move properly. A website should be accessible by all, if not, then it's classed as discrimination. The 2010 Equality Act and its predecessor the 1995 Disability Discrimination Act created a legal duty for businesses and organizations to ensure their services are available to everyone regardless of disability. While the AAA rating can't be reasonably achieved (by most websites) the following should be considered good guidence.

## Visual and informational
- Colors of text should be at least AA accessible in all cases. Critical information should be AAA - 4.5:1 & 3:1
- Font sizes (and other elements) should be set in relative units such as rems or ems not fixed as px.
- Blinking and moving text should be avoided. Moving text should not be something that is clickable.
- Images must have alt text that is descriptive of the image for screen readers and also if images fail to load. e.g. "man running over bridge"
- Links should have a title tag with additional/ advisory information e.g. '<a href=”/carl/” title=”Author’s biography”>Carl</a>'
- Links should be differentiated by more than just color (e.g. underlined or bold)
- Content should have whitespace to separate it from other content.
- Capchas must have text alternatives
- Form elements should scale and should show a max of 80 chars
- For graphical representations like graph data, make sure that you associate data with them. Tabular data works best in most cases.
- Icons cannot be the only indication of a function or warning. Screen reader friendly markup
- Elements that can be :focused must have some visual indication when focused. This includes links and buttons (same as :active)
- All CTAs / buttons must be descriptive of the action that will be triggered

## Interactivity
- Click targets and buttons should be big enough to click or tap easily (especially mobile)
- The site should be navigable by keyboard. Tab indexes should be used where needed
- Javascript interactivity must have a non-js fallback e.g. a popup should have a dedicated page for when Javascript fails, is turned off or a screenreader can't handle Javascript
- Animated elements should have a way to stop animations if longer than 5s or start automatically
- Blocks of code must be able to be bypassed e.g. Skip over nav to content
- Focus order of content must be correct
- Maps are generally not acceptable for access so alternatives should be provided - e.g. a list of routes. See the http://www.theaa.com/route-planner/index.jsp
- Error indication should be shown at a per form element level and be descriptive of the errors
- Navigation does not rely on JavaScript and rollovers.
- Screen reader markup should be applied to areas that are only for screen readers (see bootstrap docs for details)

**Extra reading:**

- https://viget.com/inspire/color-contrast
- http://www.nomensa.com/blog/2012/7-web-accessibility-myths-2
- http://heydonworks.com/practical_aria_examples/
- http://webaim.org/techniques/keyboard/tabindex
- http://webaim.org/articles/visual/lowvision
- http://www.sitepoint.com/how-many-users-need-accessible-websites/
- http://webaim.org/techniques/fonts/#font_size
- http://squizlabs.github.io/HTML_CodeSniffer/Standards/WCAG2/
- https://dequeuniversity.com/assets/html/jquery-summit/html5/slides/landmarks.html
- http://getbootstrap.com/css/#helper-classes-screen-readers
- http://webaim.org/techniques/skipnav/
- http://www.nomensa.com/blog/2010/three-rules-for-creating-accessible-forms
- http://www.alistapart.com/articles/prettyaccessibleforms/
- http://alistapart.com/article/accessibledatavisualization

**Tools**

- https://squizlabs.github.io/HTML_CodeSniffer/
